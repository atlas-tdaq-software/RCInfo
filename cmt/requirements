package RCInfo

author  Giovanna.Lehmann@cern.ch, Giuseppe.Avolio@cern.ch
manager  Giovanna.Lehmann@cern.ch

use TDAQPolicy
use is

private
branches rc
branches jsrc/rc

#-------------------------------------------------------------------------------
# Go to sw repository database
#-------------------------------------------------------------------------------

macro sw.repository.java.RCInfo.jar:name                   "RC IS structures jar"
macro sw.repository.java.RCInfo.jar:description            "jar for IS information about DAQ applications, RC states, run parameters and conditions"
macro sw.repository.is-info-file.share/data/RCInfo/daq_app_is_info.xml:name    "RC IS xml description"

#-------------------------------------------------------------------------------
# generate classes from IS info
#------------------------------------------------------------------------------

action  is_generation_j    "is_generator.sh -java -d ../jsrc/rc -p rc     ../data/daq_app_is_info.xml"

action  is_generation_c    "is_generator.sh -cpp  -d ../rc      -pd rc    ../data/daq_app_is_info.xml"

action  is_generation_jn   "is_generator.sh -java -d ../jsrc/rc -p rc -n  ../data/daq_app_is_info.xml"

action  is_generation_cn   "is_generator.sh -cpp  -d ../rc      -pd rc -n ../data/daq_app_is_info.xml"

apply_pattern build_jar \
              name="RCInfo" \
              src_dir="../jsrc/rc" \
              sources="*.java" 

macro	RCInfo.jar_dependencies	"is_generation_j is_generation_c is_generation_jn is_generation_cn"
	      
apply_pattern   javadoc   name=rc         src_dir=../jsrc/

apply_pattern install_jar         		files=RCInfo.jar
apply_pattern install_data        		src_dir="../data"   files="*.xml"
apply_pattern install_headers  name="rc"        src_dir="../rc" target_dir="../rc" files="*.h"
