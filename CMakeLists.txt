
tdaq_package()

# set(java_srcs)
# set(cpp_srcs)
tdaq_generate_isinfo(RCInfo_ISINFO JAVA_OUTPUT java_srcs SIMPLE NAMED OUTPUT_DIRECTORY jsrc/rc PACKAGE rc data/daq_app_is_info.xml)
tdaq_generate_isinfo(RCInfo_ISINFO CPP_OUTPUT  cpp_srcs  SIMPLE NAMED OUTPUT_DIRECTORY rc      PREFIX rc data/daq_app_is_info.xml)

tdaq_add_jar(RCInfo 
  SOURCES ${java_srcs} 
  INCLUDE_JARS is/is.jar ipc/ipc.jar Jers/ers.jar TDAQExtJars/external.jar)

tdaq_add_is_schema(data/daq_app_is_info.xml)

tdaq_add_jar_to_repo(RCInfo.jar
    DESCRIPTION "jar for IS information about DAQ applications, RC states, run parameters and condition")

